import $ from 'jquery';

var band;
var bandSize;
var bust;
var diff;

$(document).ready(function() {

    // $('.showCm').addClass('selected');

       $(".useCm").click(function() {
           $('.useInch').removeClass('selected');
           $('.useCm').addClass('selected');

            $('.tip-band').html('Please enter a cm band value');
            $('.tip-bust').html('Please enter a cm bust value');

           // $('.tipInch').removeClass('show');
       });

       $(".useInch").click(function() {
           $('.useInch').addClass('selected');
           $('.useCm').removeClass('selected');

            $('.tip-band').html('Please enter a inch band value');
            $('.tip-bust').html('Please enter a inch bust value');

           // $('.tipCm').removeClass('show');
           // $('.tipInch').addClass('show');
       });

 //       function showTip() {
 //        if ($('.useInch').hasClass('selected')) {
 //            $('.tipCm').html('');
 //                        $('.tipCm').html('Inches');

 //        }
 //        else
 // if ($('.useCm').hasClass('selected')) {
 //            $('.tipCm').html('Yup');
 //                        $('.tipInches').html('');

 //        }
 //        return false;
 //       }


    // FUNCTIONS FOR CM CALCULATOR

    var diff;

    // var diff = Number(band) + Number(bust);
    function getBand() {
        var band = document.getElementById("input-one").value;

        if (band == "") {
            $('.error-band').html('you have not entered your band');
        } else
      if($('.useCm').hasClass('selected')){
        //Check if entry is withhig correct range
          if (band >= 67 && band <= 104) {
            //Convert band size to inches
            band = Math.round(band / 2.54);

            //Hide Error message if condition is true
            $('.error-band').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no band');
            $('.error-band').html('Please Enter a valid band...');

            return false;

            $('.here').hide();
            $('.here-more').hide();
        }
      } else 
      if($('.useInch').hasClass('selected')){
          if (band >= 26 && band <= 39) {
            // console.log(band);
            $('.error-band').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no band');
            $('.error-band').html('Please Enter a valid band...');


            $('.here').hide();
            $('.here-more').hide();
                        return false;

        }
      }
      console.log(band);

        return band;

    };

    function getBust() {
        var bust = document.getElementById("input-two").value;
        if (bust == "") {
            $('.error-bust').html('you have not entered your bust');
            $('.here').hide();
            $('.here-more').hide();
        } else
        if($('.useCm').hasClass('selected')){
          if (bust >= 13 && bust <= 36) {
            // console.log(bust);
                                    bust = Math.round(bust / 2.54);

            $('.error-bust').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no band');
            $('.error-bust').html('Please Enter a valid bust...');

            return false;

            $('.here').hide();
            $('.here-more').hide();
        }
      } else 
      if($('.useInch').hasClass('selected')){
          if (bust >= 5 && bust <= 14) {

            //Convert Inches to CM's
            // bust = Math.round(bust * 2.54);
                        // console.log(bust);


            $('.error-bust').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no bust');
            $('.error-bust').html('Please Enter a valid bust...');

            return false;

            $('.here').hide();
            $('.here-more').hide();
        }
      }
        console.log(bust);

        return bust;

    };

    function calDiff() {

        if (getBand() != false && getBust() != false) {
            var a = getBand();
            var b = getBust();

            if((a%2) == 0){
                bandSize = (+a) + 4;
            } else{
                bandSize = (+a) + 5;

            }

            diff = +a - +b;

            // bandSize = (+a) + 4;


            $('.here').html(bandSize);
            return bandSize;
        } else {
            console.log('No values entered');
            // $('.here').html('');
            // $('.here-more').html('');
            $('.here').hide();
            $('.here-more').hide();

        }
    };

    function assignCup() {
        if (diff < 10) {
            $('.here-more').html('a');
        } else
        if (diff < 20) {
            $('.here-more').html('b');
        }
        if (diff < 30) {
            $('.here-more').html('c');
        }
    }

    $("#calculate").click(function() {
        calDiff();
        assignCup();



    });

    // END FUNCTIONS FOR CM CALCULATOR

    $(".showCm").click(function() {
        $('.cm').show();
        $('.inch').hide();
        $('.showInch').removeClass('selected');
        $('.showCm').addClass('selected');
    });
    $(".showInch").click(function() {
        $('.inch').show();
        $('.showInch').addClass('selected');
        $('.showCm').removeClass('selected');
        $('.cm').hide();
    });

})



// $(document).ready(function() {

//     var band = $('#input-one').val();
//     var bust = $('#input-two').val();;

//     var difference = band - bust;
//     // var c = a + b;

//     console.log(a, b);

//         $("#calculate").click(function() {


//     if (band.checkValidity()) {
//         $('#bra-size').html('no');

//     } else {
//         $('#bra-size').html('great');

//     }

// }

//get band value

// $('#inbox-one').blur(function() {
//     var a = $('input[name="one"]').val();
//     console.log(a);
// });

//check value input is integer between range

//    $('#inbox-one').blur(function(){
//  //is it an interger
//  var a  = $('#input-one').val;

//  if($('#input-one').type === int) {
//  console.log('yup')}
// });

//check if both values exist

//considerations
//If we enter values they must be validated
//a dropdown could reduce errors and frustrations

//Perform calculation
// $("#calculate").click(function() {
// $('#bra-size').html(c
// const band = $('input[name="one"]').val();
// const cup = $('input[name="two"]').val();

//check if values are intergers
// if (band && cup == (int)) {
// } else {
//     $('.error').html('You have not entered a value for either');
// }

// if (band == 0 && cup == 0) {
//     $('.error').html('Please enter a value bn...');
// } else {
// console.log(band);
// console.log(cup);
//         if (band !== 0 && cup !== 0) {
//             $('.error').hide();
//         }
//     }

//     $('#bra-size').html(band + cup);

// });

//     $("#calculate").click(function() {
//     // $('#bra-size').html(c
//     if (band <= 30) {
//     $('#bra-size').html('asd;as');
//     return false;


// } else {
//     $('#bra-size').html('');


// }
//                     return true;

// });

// function calculate(){
//             $('#bra-size').html(band - bust);

// }

//         $("#calculate").click(function(){
//                                 $('#bra-size').html(band - bust);

//         });



// })