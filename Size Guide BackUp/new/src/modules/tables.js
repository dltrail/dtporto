
// Hover Functionality

$('td').on('mouseover mouseout', function() {
    $(this).prevAll().addBack()
        .add($(this).parent().prevAll()
            .children(':nth-child(' + ($(this).index() + 1) + ')')).toggleClass('hover');
});

//cm and inch switcher

   $(".showCm").click(function() {
    $('.cm').show();
    $('.inch').hide();
    $('.showInch').removeClass('selected');
    $('.showCm').addClass('selected');
});
$(".showInch").click(function() {
    $('.inch').show();
    $('.showInch').addClass('selected');
    $('.showCm').removeClass('selected');
    $('.cm').hide();
});