jQuery(document).on('ready', function() {

        var MyPlayButton = $('.playBtn');

        MyPlayButton.on('click', function() {

            if ($('.vid').get(0).paused) {
                $('.vid').get(0).play();
                $('.playBtn').text("Pause");


                console.log('play');
            } else {
                $('.vid').get(0).pause();
                $('.playBtn').text("Play");

                console.log('paused');
            }
            return false;
        });

var countDownDate = new Date("Feb 8, 2020 15:37:25").getTime();

var x = setInterval(function() {

  var now = new Date().getTime();
    
  var distance = countDownDate - now;
    
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));

    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));  

  
  document.getElementById("counter").text = hours + minutes;
    
  if (days < 1) {
    clearInterval(x);
    document.getElementById("counter").text = "1";
  }
}, 1000);


});