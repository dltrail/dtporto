//Require styles so they get put through the Webpack pipeline and get compiled
require('./styles/styles.scss');

// Wrap everything in a closure so no global variables
// Pass in jQuery to $
(function($) {

    var sliderModule = (function() {
    

 $(document).on('ready', function() {
        $('.lookbook-carousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.thumbnail-carousel',


        });
        //Script for the thumbnail carousel
        $('.thumbnail-carousel').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.lookbook-carousel',
            dots: false,
            centerMode: true,
            focusOnSelect: true,
            arrows: true,
            draggable: false,
            // centerPadding: '10',
            variableWidth: true,
            speed: 200,
            responsive: [{
                breakpoint: 600,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    draggable: false,
                    swipe: true,
                    infinite: true,
                                        centerMode: true,

                },
                breakpoint: 460,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    draggable: false,
                    swipe: true,
                    centerMode: true,
                    infinite: true,
                }
            }, ]
        });
        $('.lookbook-carousel').on('afterChange', function(event, slick, currentSlide) {
            var video = $(slick.$slides[currentSlide]).find('video');
            if (video.length > 0)
                $(video).get(0).play();
        });
        $('.lookbook-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                $("video").each(function() {
                    $(this).get(0).pause();
                });
        });

  
    });
})();

})(jQuery);