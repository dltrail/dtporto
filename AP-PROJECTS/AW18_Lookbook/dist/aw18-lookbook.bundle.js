/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
    } else {
        obj[key] = value;
    }return obj;
}

//Require styles so they get put through the Webpack pipeline and get compiled
__webpack_require__(0);

// Wrap everything in a closure so no global variables
// Pass in jQuery to $
(function ($) {

    var sliderModule = function () {

        $(document).on('ready', function () {
            var _ref;

            $('.lookbook-carousel').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                asNavFor: '.thumbnail-carousel'

            });
            //Script for the thumbnail carousel
            $('.thumbnail-carousel').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.lookbook-carousel',
                dots: false,
                centerMode: true,
                focusOnSelect: true,
                arrows: true,
                draggable: false,
                // centerPadding: '10',
                variableWidth: true,
                speed: 200,
                responsive: [(_ref = {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        draggable: false,
                        swipe: true,
                        infinite: true,
                        centerMode: true

                    }
                }, _defineProperty(_ref, 'breakpoint', 460), _defineProperty(_ref, 'settings', {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    draggable: false,
                    swipe: true,
                    centerMode: true,
                    infinite: true
                }), _ref)]
            });
            $('.lookbook-carousel').on('afterChange', function (event, slick, currentSlide) {
                var video = $(slick.$slides[currentSlide]).find('video');
                if (video.length > 0) $(video).get(0).play();
            });
            $('.lookbook-carousel').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $("video").each(function () {
                    $(this).get(0).pause();
                });
            });
        });
    }();
})(jQuery);

/***/ })
/******/ ]);