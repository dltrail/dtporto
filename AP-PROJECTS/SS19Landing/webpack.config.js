const webpack = require('webpack');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin('./styles.css');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry : __dirname + '/src/index.js',
	output : {
		path: __dirname + '/dist',
		filename: 'ss19-landing.bundle.js'
	},
	plugins : [
		/**
    	 * Uncomment for production - minify everything
		new webpack.DefinePlugin({ // <-- key to reducing React's size
      		'process.env': {
        		'NODE_ENV': JSON.stringify('production')
      		}
    	}),
    	new UglifyJsPlugin(), //minify everything
    	new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks 
    	*/
		extractSass,
		new HTMLWebpackPlugin({
			template : __dirname + '/src/index.html',
			filename : 'ss19-landing.html',
			chunksSortMode : 'none'
		})

	],
	externals : {
		jquery : 'jQuery',
	},
	module : {
		rules : [
			{
				test : /\.js/, 
				exclude : /node_modules/, 
				loader : 'babel-loader',
				query: {
				  presets: ['env', 'react']
				}
			},
			{
			        test: /\.json$/,
			        loader: 'json-loader'
			},
			{
            	test: /\.scss$/,
            	use: extractSass.extract({
               		use: [{
                    	loader: "css-loader"
                	}, {
                    	loader: "sass-loader",
                    	options : {
                    		outputStyle : 'expanded'
                    	}
                	}]
	            })
	        },
	        {
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
				  presets: ['env', 'react']
				}
			}
		]
	},
	devServer : {
		contentBase : __dirname + '/dist',
		https : true
	}
}

