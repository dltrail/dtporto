
// Wrap everything in a closure so no global variables
// Pass in jQuery to $
(function($) {

    var sliderModule = (function() {
     $(document).on('ready', function() {

    $('.triplet-carousel').slick({
        infinite: true,
        speed: 8,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<div class="slick-arrow slick-prev"></div>',
        nextArrow: '<div class="slick-arrow slick-next"></div>',

        speed: 200,
        cssEase: 'linear',
        centerPadding: '150px',
        swipeToSlide: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                draggable: true,
                swipe: true,
                infinite: false,

            }
        }, ]
    });

    $('.single-carousel').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        speed: 200,
        cssEase: 'linear',
        prevArrow: '<div class="slick-arrow slick-prev"></div>',
        nextArrow: '<div class="slick-arrow slick-next"></div>',

    });

    $('.bts-carousel').slick({
        slidesToShow: 3,
        variableWidth: true,
        dots: false,
        arrows: false,
        // draggable: true,
        infinite: true,
        swipe: true,
        swipeToSlide: true,
        centerMode: true,

        focusOnSelect: true,
        speed: 600,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 2,

                dots: false,
                arrows: false,
                // draggable: true,
                infinite: true,
                swipe: true,
                swipeToSlide: true,

                // centerMode: true,
                focusOnSelect: true,
                speed: 600,
                cssEase: 'linear',

            },
            breakpoint: 767,
            settings: {
                slidesToShow: 2,

                dots: false,
                arrows: false,
                // draggable: true,
                infinite: true,
                swipe: true,
                swipeToSlide: true,

                // centerMode: true,
                focusOnSelect: true,
                speed: 600,
                cssEase: 'linear',

            }
        }, ]
    });

    $('.readBtn').click(function() {
        console.log('Banner Read More');
        $('.more').toggle('easing',function() {

            if ($('.more').css('display') == 'none') {
                $('.readBtn').text('Read More');
            } else {
                $('.readBtn').text('Read Less');

            }
        });

    });

     $('#readBtn').click(function() {
            console.log('Banner Read More');
            var x = document.getElementById('more');
            var y = document.getElementById('readBtn');
            var z = document.getElementById('catElipses');
            if (x.style.display === "inline") {
                x.style.display = "none";
                y.innerHTML = "Read more";
                z.style.display = "inline";

            } else {
                x.style.display = "inline";
                y.innerHTML = "Read less";
                z.style.display = "none";
            }
        });

    // $('.readBtn-one').click(function() {
    //     console.log('Banner Read More');
    //     $('.more-one').toggle('slow', function() {

    //         if ($('.more-one').css('display') == 'none') {
    //             $('.readBtn-one').text('Read More');
    //         } else {
    //             $('.readBtn-one').text('Read Less');

    //         }
    //     });

    // });

    //   $('.readBtn-two').click(function() {
    //     console.log('Banner Read More');
    //     $('.more-two').toggle('slow', function() {

    //         if ($('.more-two').css('display') == 'none') {
    //             $('.readBtn-two').text('Read More');
    //         } else {
    //             $('.readBtn-two').text('Read Less');

    //         }
    //     });

    // });

    $('.parallax').click(function()
    {
        window.location = "https://www.agentprovocateur.com/gb_en/lingerie/pump-it-up";

    })

     $('.rm_parallax').click(function()
    {
        window.location = "https://www.agentprovocateur.com/gb_en/lingerie/pump-it-up";

    })

// $('#campaign_video').each(function(){
//     if ($(this).is(":in-viewport")) {
//         $(this)[0].play();
//     } else {
//         $(this)[0].pause();
//     }
// })

  
    });
})();

})(jQuery);