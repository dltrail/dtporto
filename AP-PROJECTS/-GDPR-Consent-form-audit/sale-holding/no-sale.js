	(function($) {

    var countryCode;

    if (window.location.href.indexOf('gb_en') >= 0) {
        countryCode = 'United Kingdom';
    } else if (window.location.href.indexOf('eu_en') >= 0) {
        countryCode = 'Europe';
    } else if (window.location.href.indexOf('us_en') >= 0) {
        countryCode = 'United States'
    } else if (window.location.href.indexOf('int_en') >= 0) {
        countryCode = 'International';
    } else {
        countryCode = 'United Kingdom';
    }

    var cookies = {

        create: function(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toUTCString();
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        },

        read: function(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        },

        erase: function(name) {
            createCookie(name, "", -1);
        }

    };

    var formModule = (function(module) {

        let _mode = 'forced';

        function mode(formMode = _mode) {

            _mode = formMode;

            return _mode;
        }

          function validate(email) {

            const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (!emailRegEx.test(email)) {

                showError('email');
                return false;

            } else {
                $('#errorMessageContainerId').fadeOut();
                document.getElementById("control_EMAIL").style.borderColor = "black";
            }
            return true;
        };

                     function isChecked() {
    var x = document.getElementById("control_COLUMN143_2").checked;
    if (x == false) {
          document.getElementById("terms-error").innerHTML = "You must accept terms and conditions";
          document.getElementById("terms-error").style.borderColor = "red";
          return false;
    } else {
                document.getElementById("terms-error").innerHTML = "";
            }
            return true;
        };

//         function isChecked()
// {
//  if(document.getElementById("control_COLUMN143_2").is(':checked'))){
//      showError('checked');
//      return false
//  }
// }
        function showError(err) {

            if (err === 'email') {
                $('#errorMessageContainerId').fadeIn();
                document.getElementById("control_EMAIL").style.borderColor = "#dc153d";
            }
            //  if (err === 'checked') {
            //     $('#errorMessageContainerId').fadeIn();
            //     document.getElementById("control_COLUMN143_2").style.borderColor = "#dc153d";
            // }
        };

        function getCountryCode() {
            jQuery('#control_COLUMN118').val(countryCode);
        };

        function setDate() {
            const date = new Date();
            let mm = date.getMonth() + 1;
            if (mm <= 9) {
                mm = '0' + mm;
            }
            let dd = date.getDate();
            if (dd <= 9) {
                dd = '0' + dd;
            }
            let yyyy = date.getFullYear();
            const str = mm + '/' + dd + '/' + yyyy;
            $('#control_COLUMN165').val(str);

        };

        function setConsentDate() {
            const date = new Date();
            let mm = date.getMonth() + 1;
            if (mm <= 9) {
                mm = '0' + mm;
            }
            let dd = date.getDate();
            if (dd <= 9) {
                dd = '0' + dd;
            }
            let yyyy = date.getFullYear();
            const str = mm + '/' + dd + '/' + yyyy;
            $('#control_COLUMN188').val(str);

        };

        function getGender() {
            let gender = $('input[name=Gifting]:checked').val();
            $('input[name=Gender_SEGMENT]').val(gender);
            return gender;
        }

        function handleSubmit() {
            //check for validation

            const $form = $('.no-sale form');

            $form.submit(function(event) {

                const email = $('#control_EMAIL').val();

                const gender = getGender();
                isChecked() === true;


                if (!validate(email) || !isChecked()) {

                    event.preventDefault();

                } else {

                    setDate();

                    setConsentDate();

                    getCountryCode();


                    _satellite.track("No Sale - Data Capture");

                    cookie.create('tp_signed_up', '1', 365);

                    $form.fadeOut(500, function() {});

                    jQuery('#loader').fadeIn(500, function() {
                        return true;
                    });

                }

            });

        };

        function init() {

            handleSubmit();

        };

        return {
            init: init,
            mode: mode
        };

    })();

    formModule.init();

})(jQuery);