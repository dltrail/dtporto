var band;
var bandSize;
var bust;
var diff;



 function getBand() {
        var band = document.getElementById("input-one").value;

        if (band == "") {
            $('.error-band').html('you have not entered your band');
        } else
      if($('.useCm').hasClass('selected')){
        //Check if entry is withhig correct range
          if (band >= 67 && band <= 104) {
            //Convert band size to inches
            band = Math.round(band / 2.54);

            //Hide Error message if condition is true
            $('.error-band').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no band');
            $('.error-band').html('Please Enter a valid band...');

            return false;

            $('.here').hide();
            $('.here-more').hide();
        }
      } else 
      if($('.useInch').hasClass('selected')){
          if (band >= 26 && band <= 39) {
            // console.log(band);
            $('.error-band').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no band');
            $('.error-band').html('Please Enter a valid band...');


            $('.here').hide();
            $('.here-more').hide();
                        return false;

        }
      }
      console.log(band);

        return band;

    };

    function getBust() {
        var bust = document.getElementById("input-two").value;
        if (bust == "") {
            $('.error-bust').html('you have not entered your bust');
            $('.here').hide();
            $('.here-more').hide();
        } else
        if($('.useCm').hasClass('selected')){
          if (bust >= 13 && bust <= 36) {
            // console.log(bust);
                                    bust = Math.round(bust / 2.54);

            $('.error-bust').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no band');
            $('.error-bust').html('Please Enter a valid bust...');

            return false;

            $('.here').hide();
            $('.here-more').hide();
        }
      } else 
      if($('.useInch').hasClass('selected')){
          if (bust >= 5 && bust <= 14) {

            //Convert Inches to CM's
            // bust = Math.round(bust * 2.54);
                        // console.log(bust);


            $('.error-bust').html('');
            $('.here').show();
            $('.here-more').show();

        } else {
            // console.log('no bust');
            $('.error-bust').html('Please Enter a valid bust...');

            return false;

            $('.here').hide();
            $('.here-more').hide();
        }
      }
        console.log(bust);

        return bust;

    };

    function calDiff() {

        if (getBand() != false && getBust() != false) {
            var a = getBand();
            var b = getBust();

            if((a%2) == 0){
                bandSize = (+a) + 4;
            } else{
                bandSize = (+a) + 5;

            }

            diff = +a - +b;

            // bandSize = (+a) + 4;


            $('.here').html(bandSize);
            return bandSize;
        } else {
            console.log('No values entered');
            // $('.here').html('');
            // $('.here-more').html('');
            $('.here').hide();
            $('.here-more').hide();

        }
    };

    function assignCup() {
        if (diff < 10) {
            $('.here-more').html('a');
        } else
        if (diff < 20) {
            $('.here-more').html('b');
        }
        if (diff < 30) {
            $('.here-more').html('c');
        }
    }

    $("#calculate").click(function() {
        calDiff();
        assignCup();
    });


