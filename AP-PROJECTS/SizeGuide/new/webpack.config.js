const webpack = require('webpack');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin('./styles.css');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const HTMLWebpackPlugin = require('html-webpack-plugin');

const $ = require('jquery');

module.exports = {
  entry : __dirname + '/src/sizing.js',
  output : {
    path: __dirname + '/dist',
    filename: 'ap-sizing.bundle.js'
  },

  plugins : [

    new webpack.DefinePlugin({ // <-- key to reducing React's size
          'process.env': {
            'NODE_ENV': JSON.stringify('production')
          }
      }),
    new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),     
      new UglifyJsPlugin(), //minify everything
      new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks 

      
 extractSass,
      new HTMLWebpackPlugin({
      template : __dirname + '/src/sizing-guide-landing.html',
      filename : 'index-landing.html',
      chunksSortMode : 'none'
    }),
    new HTMLWebpackPlugin({
      template : __dirname + '/src/sizing-guide-calculator.html',
      filename : 'index-calculator.html',
      chunksSortMode : 'none'
    }),
    new HTMLWebpackPlugin({
      template : __dirname + '/src/sizing-guide-tables.html',
      filename : 'index-tables.html',
      chunksSortMode : 'none'
    }),
    new HTMLWebpackPlugin({
      template : __dirname + '/src/sizing-guide-bra-shapes.html',
      filename : 'index-bra-shapes.html',
      chunksSortMode : 'none'
    })

    ],

  module : {
    rules : [
      {
        test : /\.js/, 
        exclude : /node_modules/, 
        loader : 'babel-loader',
        query: {
          presets: ['env', 'react']
        }
      },
      {
              test: /\.json$/,
              loader: 'json-loader'
      },
      {
              test: /\.scss$/,
              use: extractSass.extract({
                  use: [{
                      loader: "css-loader"
                  }, {
                      loader: "sass-loader",
                      options : {
                        outputStyle : 'expanded'
                      }
                  }]
              })
          },
          {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['env', 'react']
        }
      }
    ]
  },
  devServer : {
    contentBase : __dirname + '/dist',
    https : true
  }
}