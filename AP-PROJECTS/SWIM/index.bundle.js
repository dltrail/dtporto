//Require styles so they get put through the Webpack pipeline and get compiled

// Wrap everything in a closure so no global variables
// Pass in jQuery to $
(function($){

	//Flag used to indicate whether the user is actively dragging the carousel.
	//This is used by a couple modules so I've included it out here so they all have access to it
	var _isDragging = false;

	// This is a module, written using the 'Revealing Module Pattern'
	// It will return an object where we explicitly declare which methods will be available. Everything else is wrapped inside a closure so everything is private within the module.

	var ColourChange = (function(){

		/**
		 * An array of objects, which will contain information about the height offsets to switch colours at
		 * and what colour to switch to.
		 */

		var _backgroundChanges = [];
	
		/**
		 * Returns an array of background changes to make.
		 * This will iterate over every <section> tag, and check whether it has a data-background attribute.
		 * If it does, it will push an object to the array, with each object containing 
		 *  1. the colour the background should change to, and
		 * 	2. the top offset of that element (i.e. the point where the background should change), and 
		 *  3. the bottom of the element
		 */

		function getBackgroundChanges(){

			//this is the array we'll return
			var output = [];

			document.querySelectorAll('section').forEach(function(section){

				var backgroundColour = section.getAttribute('data-background')

				//if the section has an attribute called 'data-background'
				if (backgroundColour) {

					// then we push a background change object to the output array
					output.push({
						colour : backgroundColour,
						top : section.offsetTop,
						bottom : section.offsetTop + section.clientHeight - 1
					});
				}
			});

			return output;
		}

		function handleScroll(scrollTop){
			
			// iterate over the backgroundChanges array and find the currently 'active' section
			// we'll know which is active because the scrollTop will be greater than its top, but less than its bottom.

			var activeSection = _backgroundChanges.length && _backgroundChanges.filter(function(section){
				
				//this should only ever return one result...
				return scrollTop - 50 >= section.top && scrollTop - 50 < section.bottom;

			})[0];

			//if we found a section that matched the current scrollTop
			if (activeSection) {
				document.querySelector('#swim-lp-wrapper').style.backgroundColor = activeSection.colour;
			}

		}

		function init(){

			//get the initial backgroundChanges
			_backgroundChanges = getBackgroundChanges();

			//Add a resize event listener, so we can recalculate section heights and offsets etc. if they change size
			window.addEventListener('resize', function(){

				_backgroundChanges = getBackgroundChanges();

			});

		}


		// Finally, we return an object that explicitly references the functions and variables that we want to make public.
		// In this case, we only need to reveal the init function
		return {
			init : init,
			handleScroll : handleScroll
		}
		
	})();



	/**
	 * FadeIn Module
	 * This will cause the image blocks on the active section to fade in
	 */

	var FadeImagesIn = (function(){

		var _sectionImages = [];

		/**
		 * This function will cache an array of the various sfadable images (i.e. elements with a class of fadable)
		 * along with their top and bottom offsets, so we can check what needs to be faded in as we scroll along the page
		 * Cacheing these is more efficient than checking what needs to be faded every time we scroll as we only need to do it once
		 */

		function getSectionImages(){

			//this is the array we'll return
			var output = [];

			document.querySelectorAll('section').forEach(function(section){

				const fadable = section.querySelectorAll('.fadable');
				
				output.push({
					fadable : fadable,
					top : section.offsetTop,
					bottom : section.offsetTop + section.clientHeight - 1
				});

			});

			return output;
		}


		function handleScroll(scrollTop) {

			var activeSection = _sectionImages.length && _sectionImages.filter(function(section){
				
				//this should only ever return one result...
				return scrollTop + 550 >= section.top && scrollTop + 550 < section.bottom;

			})[0];

			//if we found a section that matched the current scrollTop
			if (activeSection) {
				activeSection.fadable.forEach(function(img){
					img.classList.add('faded-in');
				});
			}

		}

		function init(){

			_sectionImages = getSectionImages();

		}

		return {
			init : init,
			handleScroll : handleScroll
		}

	})();

var FormModule = (function(){

		var _formIsOpen = false;

		  var countryCode;

		  if (window.location.href.indexOf('gb_en') >= 0) {
		    countryCode = 'United Kingdom';
		  }
		  else if (window.location.href.indexOf('eu_en') >= 0) {
		    countryCode = 'Europe';
		  }
		  else if (window.location.href.indexOf('us_en') >= 0) {
		    countryCode = 'United States'
		  }
		  else if (window.location.href.indexOf('int_en') >= 0) {
		    countryCode = 'International';
		  }
		  else {
		    countryCode = 'United Kingdom';
		  }

		/**
		 * Binds click listeners for the slide elements (which will open the form)
		 * First it checks whether the user is dragging the carousel
		 * Then sets the current product based on clicked element
		 * Finally calls openForm() if the form isn't already open
		 */

		function bindOpenFormListeners(){

			//Get the slides
			const slides = document.querySelectorAll('.ss18-launch__open-form');

			// Iterate over the slides and add a click handler to each

			for (var i = 0; i < slides.length; i++){
				
				var slide = slides[i];

				function handleClick(thisSlide){

					return function(){

						// If the user is dragging the carousel, that propagate up as a 'click' on the slide and the form will open just from dragging
						// So we check if the user is actively dragging the slide before firing setCurrentProduct
						
							if (!_formIsOpen) {

								openForm();

							}
					}

				}

				slide.addEventListener('click', handleClick(slide));
			}
		}

		/**
		 * Fades the form in
		 */

		function openForm(){

			$('.ss18-launch__form').fadeIn(500, function(){
				_formIsOpen = true;
			});

		}

		/**
		 * Fades the form out
		 */

		function closeForm(){

			$('.ss18-launch__form').fadeOut(500, function(){
				$('.ss18-launch__form-wrapper').removeClass('errored');
				_formIsOpen = false;
			});

		}

		/**
		 * Binds event listeners for the various ways you can close the form
		 * i.e. clicking the X, clicking outside the form, pressing escape
		 */

		function bindCloseFormListeners(){

			//bind the 'Close' X
			$('#close-x').on('click', function(){

				if(_formIsOpen) {
					closeForm();
				}

			});


			//also bind close if they click outside the form
			$('body').on('click', function(event){

				if (_formIsOpen && !$(event.target).closest('.ss18-launch__form-wrapper').length) {
					closeForm();
				}

			})

			//finally, close the form if user presses escape
			$(document).keydown(function(e) { 

			    if (e.keyCode == 27) { 
			        if (_formIsOpen && !$(event.target).closest('.ss18-launch__form-wrapper').length) {
			        	closeForm();
			        }
			    } 

			});
		}

		/**
		 * Sets the 'Website' field on the form, based on user location
		 * This is required by the Silverpop backend
		 */

		function showError(){
			$('.ss18-launch__form-wrapper').addClass('errored');
			$('#sp-errormessage').text("You must enter a valid email address");
		}

		function validate(email){

			var emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

			if (!emailRegEx.test(email)) {
				
				showError();
				return false;

			}

			return true;
		};

		function setWebsite(){
			jQuery('#control_COLUMN118').val(countryCode);
		};

		function getGender(){
			let gender = $('input[name=Gifting]:checked').val();
			$('input[name=Gender_SEGMENT]').val(gender);
			return gender;
		}

		function setDate(){
			const date = new Date();
			let mm = date.getMonth()+1;
			if (mm <=9){
				mm = '0'+mm;
			}
			let dd = date.getDate();
			if (dd <=9) {
				dd='0'+dd;
			}
			let yyyy = date.getFullYear();
			const str = mm+'/'+dd+'/'+yyyy;
			$('#control_COLUMN72').val(str);
		};

		function handleSubmit(event){
			const gender = getGender();
			if (!validate($('#control_EMAIL').val())){
				event.preventDefault();
				return false;
			}

			// handle defaults for checkbox because Silverpop expects a literal value of 'No' even if checkbox is not ticked ಠ_ಠ
			if ($('#opt-in-yes').prop('checked')) {
				$('#opt-in-no').prop('disabled', true);
			}
			
			return true;
		}

		function init(){

			setWebsite();
			setDate();

			bindOpenFormListeners();
			bindCloseFormListeners();

			$('.ss18-launch__form-wrapper form').on('submit', function(event){
				handleSubmit(event);
			})
		}

		return {
			init : init
		}

	})();

	/**
	 * Let's init all our modules and add event listeners
	 */


	FadeImagesIn.init();


	FormModule.init();



	//Fire the scroll event once to initialize on page load
	var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
	ColourChange.handleScroll(scrollTop);
	FadeImagesIn.handleScroll(scrollTop);



	/**
	 * Add a scroll event listener to the window and fire our handleScroll functions frmo their modules
	 */

	window.addEventListener('scroll', function(){

		//multibrowser polyfill for scrollTop
		var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;

		ColourChange.handleScroll(scrollTop);
		FadeImagesIn.handleScroll(scrollTop);

	})

});