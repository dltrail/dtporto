(function($) {
    // Page smooth scroll
    var $ = jQuery;

    $(document).ready(function(){
  $("a").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        window.location.hash = hash;
      });
    } 
  });
});
})(jQuery);

function myFunction() {
    var x = document.getElementById("extra-items");
    var y = document.getElementById("see-more-btn");
    if (x.style.display === "block") {
        x.style.display = "none";
        y.innerHTML = "See more...";
    } else {
        x.style.display = "block";
        y.innerHTML = "See less...";
    }
}

           // When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("back-to-top-btn").style.display = "block";
    } else {
        document.getElementById("back-to-top-btn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}