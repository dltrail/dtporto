/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
    } else {
        obj[key] = value;
    }return obj;
}

// Wrap everything in a closure so no global variables
// Pass in jQuery to $
(function ($) {

    var sliderModule = function () {
        $(document).on('ready', function () {
            var _$$slick, _ref;

            $('.triplet-carousel').slick((_$$slick = {
                infinite: true,
                speed: 8,
                slidesToShow: 3,
                slidesToScroll: 1,
                prevArrow: '<div class="slick-arrow slick-prev"></div>',
                nextArrow: '<div class="slick-arrow slick-next"></div>'

            }, _defineProperty(_$$slick, 'speed', 200), _defineProperty(_$$slick, 'cssEase', 'linear'), _defineProperty(_$$slick, 'centerPadding', '150px'), _defineProperty(_$$slick, 'swipeToSlide', true), _defineProperty(_$$slick, 'responsive', [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    draggable: true,
                    swipe: true,
                    infinite: false

                }
            }]), _$$slick));

            $('.single-carousel').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                speed: 200,
                cssEase: 'linear',
                prevArrow: '<div class="slick-arrow slick-prev"></div>',
                nextArrow: '<div class="slick-arrow slick-next"></div>'

            });

            $('.bts-carousel').slick({
                slidesToShow: 3,
                variableWidth: true,
                dots: false,
                arrows: false,
                // draggable: true,
                infinite: true,
                swipe: true,
                swipeToSlide: true,
                centerMode: true,

                focusOnSelect: true,
                speed: 600,
                cssEase: 'linear',
                responsive: [(_ref = {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,

                        dots: false,
                        arrows: false,
                        // draggable: true,
                        infinite: true,
                        swipe: true,
                        swipeToSlide: true,

                        // centerMode: true,
                        focusOnSelect: true,
                        speed: 600,
                        cssEase: 'linear'

                    }
                }, _defineProperty(_ref, 'breakpoint', 767), _defineProperty(_ref, 'settings', {
                    slidesToShow: 2,

                    dots: false,
                    arrows: false,
                    // draggable: true,
                    infinite: true,
                    swipe: true,
                    swipeToSlide: true,

                    // centerMode: true,
                    focusOnSelect: true,
                    speed: 600,
                    cssEase: 'linear'

                }), _ref)]
            });

            $('.readBtn').click(function () {
                console.log('Banner Read More');
                $('.more').toggle('easing', function () {

                    if ($('.more').css('display') == 'none') {
                        $('.readBtn').text('Read More');
                    } else {
                        $('.readBtn').text('Read Less');
                    }
                });
            });

            $('#readBtn').click(function () {
                console.log('Banner Read More');
                var x = document.getElementById('more');
                var y = document.getElementById('readBtn');
                var z = document.getElementById('catElipses');
                if (x.style.display === "inline") {
                    x.style.display = "none";
                    y.innerHTML = "Read more";
                    z.style.display = "inline";
                } else {
                    x.style.display = "inline";
                    y.innerHTML = "Read less";
                    z.style.display = "none";
                }
            });

            // $('.readBtn-one').click(function() {
            //     console.log('Banner Read More');
            //     $('.more-one').toggle('slow', function() {

            //         if ($('.more-one').css('display') == 'none') {
            //             $('.readBtn-one').text('Read More');
            //         } else {
            //             $('.readBtn-one').text('Read Less');

            //         }
            //     });

            // });

            //   $('.readBtn-two').click(function() {
            //     console.log('Banner Read More');
            //     $('.more-two').toggle('slow', function() {

            //         if ($('.more-two').css('display') == 'none') {
            //             $('.readBtn-two').text('Read More');
            //         } else {
            //             $('.readBtn-two').text('Read Less');

            //         }
            //     });

            // });

            $('.parallax').click(function () {
                window.location = "https://www.agentprovocateur.com/gb_en/lingerie/pump-it-up";
            });

            $('.rm_parallax').click(function () {
                window.location = "https://www.agentprovocateur.com/gb_en/lingerie/pump-it-up";
            });

            // $('#campaign_video').each(function(){
            //     if ($(this).is(":in-viewport")) {
            //         $(this)[0].play();
            //     } else {
            //         $(this)[0].pause();
            //     }
            // })
        });
    }();
})(jQuery);

/***/ })
/******/ ]);